# urxvt-theme

![Screenshot](screenshot.png)

URxvt-theme is an extension for the rxvt-unicode terminal emulator that adds support for on the fly color scheme selection. It provides a popup menu bound to *Control + Right Click* which lists all the themes currently available in your X resources.

To add a new theme simply edit your *.Xresources* file in this way:
```
URxvt.theme.Solarized.background: #fdf6e3
URxvt.theme.Solarized.foreground: #657b83
URxvt.theme.Solarized.color0: #073642
! [...]
URxvt.theme.Solarized.color15: #fdf6e3
```
To install this extension put the *theme* file in your *~/.urxvt/ext/* directory and append `theme` to `URxvt.perl-ext-common` in your *.Xresources*.

Remember to run *xrdb* after making changes in your X resources file.

